---
title: 使用docker部署wordpress
date: 2017-12-06 14:59:33
tags: [wordpress,docker]
categories: 后端服务器
---



之前记录了直接在服务器上部署wordpress的方法，后来发现如果用docker，就能用到docker的各种好处，果断尝试。



我的服务器还是ubuntu16.04

#### 先安装docker

```shell
sudo apt install docker.io
```

#### 文件目录准备

在需要的地方创建wordpress目录，里面之后会有一些文件目录挂载到docker里，实现docker里文件的持久化

```shell
mkdir  ~/wordpress-compose && cd ~/wordpress-compose
```

#### 编写Dockerfile

在wordpress-compose目录下

```shell
vi Dockerfile
```

里面写入内容,因为需要php的依赖，否则wordpress起不来，这是一个坑，然后创建了code目录

```
FROM orchardup/php5
ADD . /code
```

#### 编写docker-compose.yml

```shell
vi docker-compose.yml
```

里面的内容是两个容器的配置，注意yml文件需要严格的缩进格式。需要注意的是端口的配置和数据卷的挂载。

```
wordpress:
    image: wordpress
    links:
     - mariadb:mysql
    environment:
     - WORDPRESS_DB_PASSWORD=你的密码
    ports:
     - "<server public IP>:80:80"
    volumes:
     - ./code:/code
     - ./html:/var/www/html
mariadb:
    image: mariadb
    ports:
     - "33060:3306"
    environment:
     - MYSQL_ROOT_PASSWORD=你的密码
     - MYSQL_DATABASE=wordpress
    volumes:
     - ./database:/var/lib/mysql
```



#### 用配置文件开始部署docker

```shell
docker-compose up -d
```

查看在跑的docker容器

```shell
docker ps
```

这样以后如果要迁移wordpress，直接移动外面的wordpress-compose目录就行，非常方便，所有数据都在里面了。