---
title: docker部署es-kibana-apm
date: 2021-03-27 14:03:28
tags:[docker,apm,elastic]
---

##### 到es官网复制一份docker-compose.yml

[https://www.elastic.co/guide/en/apm/get-started/current/quick-start-overview.html]

##### 执行的时候需要使用环境变量指定APM的地址


```yaml
ELASTIC_APM_SERVER_URL=http://localhost:8200
```

```go
	apm.DefaultTracer.Service.Name = opt.Name
	apm.DefaultTracer.Service.Environment = opt.Env
	apm.DefaultTracer.Service.Version = opt.Version
```

