---
title: ubuntu双显卡自动安装nvidia显卡驱动
date: 2019-01-19 10:20:40
tags: [ubuntu,nvidia,双显卡]
categories : 人工智能
---

最近在玩深度学习，正好手上有一台Nvidia显卡的游戏本。于是装个ubuntu18.04玩玩。装官方驱动遇到了双显卡导致的显卡驱动的各种问题。后来一顿操作，发现其实可以几行代码搞定，不需要像网上流传的那么繁琐。。

装好系统后，首先要进去桌面，这里网上有常见的办法，开机设置 nomodeset 就好

进入桌面后，联网，换源。



1. 首先，检测你的NVIDIA显卡型号和推荐的驱动程序的模型，并显示计算机即将安装的驱动。

1. ```shell
   ubuntu-drivers devices
   ```

2. 如果同意计算机的以上推荐设置，执行

   ```shell
   sudo ubuntu-drivers autoinstall
   ```

   接下来就是等待，装好后重启。