---
title: ssh连接长时间闲置不断线
date: 2018-09-03 18:14:49
tags: [ssh]
categories: 后端服务器
---

ssh 连接上长时间空闲不掉线，省去重新登陆的麻烦事情。在本地创建 ～/.ssh/config 文件，Host可以写全指定主机，ServerAliveInterval是多久发心跳确认连接，ServerAliveCountMax为保持连接的最大客户端数量。

```shell
Host *
ServerAliveInterval 60 
ServerAliveCountMax 16
```

