---
title: Vagrant初体验
date: 2018-08-01 17:53:44
tags: [vagrant]
categories: 工具
---



## Vagrant初体验

想要配一套集成开发环境，这样不会影响我电脑本地的其他配置，选择了Vagrant。

#### 准备材料

```
提示
Vagrant box: 一个打包好的操作系统，是一个后缀名为 `.box` 的文件，其实是一个压缩包，里面包含了 Vagrant 的配置信息和 VirtualBox 的虚拟机镜像文件。说白了就是个虚拟机系统文件，跟装windows虚拟机的ghost类似
```

1. VirtualBox安装包 https://www.virtualbox.org/wiki/Downloads
2. Vagrant安装包 https://www.vagrantup.com/downloads.html
3. Vagrant box 文件 http://www.vagrantbox.es/ ，官网下载慢点话，网上有人分享了一个https://pan.baidu.com/s/1wJCeWEyxKQLVPi1IH1IlYg ，我也是用的这个 ubuntu-server-16.04



#### 安装

1. 安装 VirtualBox，因为Vagrant的启动依赖于它。这是前提。如果是mac系统，可能需要到系统偏好设置->安全性与隐私里打开相关权限。不然安装失败。

2. 这一步你已经成功安装了VirtualBox，然后进到你的开发目录下

   ```shell
   mkdir vagrant   ###新建文件夹			
   ```

3. 把下载的box文件放到vagrant目录下，在vagrant下执行命令，添加前面下载的 `box` 到vagrant
   命令格式：`vagrant box add <取个本地box名称> <box 文件>`

   ```shell
   ## 添加
   vagrant box add ubuntu-server-16.04 ubuntu-server-16.04-amd64-vagrant.box
   ##查看是否添加成功
   vagrant box list
   ```

4. 初始化,这个时候会在当前目录生成配置文件Vagrantfile

   ```shell
   vagrant init 'ubuntu-server-16.04'
   ```

5. 启动和连接虚拟机

   ```shell
   ##启动
   vagrant up 
   ## 连接
   vagrant ssh
   ```

####常用命令

如果有多个box，指定box的操作就在对应命令后加box名字

```shell
vagrant init      # 初始化，生成Vagrantfile

vagrant up        # 启动虚拟机
vagrant halt      # 关闭虚拟机
vagrant reload    # 重启虚拟机
vagrant ssh       # SSH 至虚拟机
vagrant suspend   # 挂起虚拟机
vagrant resume    # 唤醒虚拟机
vagrant status    # 查看虚拟机运行状态
vagrant destroy   # 销毁当前虚拟机
vagrant package --output myvagrant_image.box # 将当前虚拟机打成镜像

#box管理命令
vagrant box list    # 查看本地box列表
vagrant box add     # 添加box到列表
vagrant box remove  # 从box列表移除 

# 修改了配置需要启动或重启
vagrant provision
vagrant reload --provision
```



#### 使用说明

虚拟机的主要配置文件是Vagrantfile，第一次init后自动生成，可以自己修改。复制开发环境给别人时只要给Vagrantfile和box文件就行。具体的Vagrantfile还得细细学习。