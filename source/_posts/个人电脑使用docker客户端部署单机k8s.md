---
title: 个人电脑使用docker客户端部署单机k8s
date: 2020-7-26 11:10:42 
tags: [k8s,docker]
categories: 后端服务器
---

#### docker 客户端安装k8s
docker 客户端自带的k8s 工具一直在starting
这是因为对应的包下不下来。首先自己查看一下自己k8s的版本。
Kubernetes   v1.18.8 用的docker镜像是下面这些，手动下载就行

如果是其他版本k8s，这个仓库有处理方案：https://github.com/AliyunContainerService/k8s-for-docker-desktop

```shell
docker pull naison/kube-proxy:v1.18.8
docker pull naison/kube-apiserver:v1.18.8
docker pull naison/kube-controller-manager:v1.18.8
docker pull naison/kube-scheduler:v1.18.8
docker pull naison/weave-npc:2.7.0
docker pull naison/weave-kube:2.7.0
docker pull naison/pause:3.2
docker pull naison/coredns:1.6.7
docker pull naison/etcd:3.4.3-0
 
docker tag  docker.io/naison/kube-proxy:v1.18.8                 k8s.gcr.io/kube-proxy:v1.18.8
docker tag  docker.io/naison/kube-apiserver:v1.18.8             k8s.gcr.io/kube-apiserver:v1.18.8
docker tag  docker.io/naison/kube-controller-manager:v1.18.8    k8s.gcr.io/kube-controller-manager:v1.18.8
docker tag  docker.io/naison/kube-scheduler:v1.18.8             k8s.gcr.io/kube-scheduler:v1.18.8
docker tag  docker.io/naison/weave-npc:2.7.0                    docker.io/weaveworks/weave-npc:2.7.0
docker tag  docker.io/naison/weave-kube:2.7.0                   docker.io/weaveworks/weave-kube:2.7.0
docker tag  docker.io/naison/pause:3.2                          k8s.gcr.io/pause:3.2
docker tag  docker.io/naison/coredns:1.6.7                      k8s.gcr.io/coredns:1.6.7
docker tag  docker.io/naison/etcd:3.4.3-0                       k8s.gcr.io/etcd:3.4.3-0
 
docker image rm  docker.io/naison/kube-proxy:v1.18.8
docker image rm  docker.io/naison/kube-apiserver:v1.18.8
docker image rm  docker.io/naison/kube-controller-manager:v1.18.8
docker image rm  docker.io/naison/kube-scheduler:v1.18.8
docker image rm  docker.io/naison/weave-npc:2.7.0
docker image rm  docker.io/naison/weave-kube:2.7.0
docker image rm  docker.io/naison/pause:3.2
docker image rm  docker.io/naison/coredns:1.6.7
docker image rm  docker.io/naison/etcd:3.4.3-0
```


#### 然后安装k8s的dashboard
https://kubernetes.io/zh/docs/tasks/access-application-cluster/web-ui-dashboard/

https://www.qikqiak.com/k8s-book/docs/17.%E5%AE%89%E8%A3%85%20Dashboard%20%E6%8F%92%E4%BB%B6.html

