---
title: mysql开启远程访问
date: 2017-12-06 15:58:02
tags: mysql
categories: 后端服务器
---



#### 首先查看3306端口是不是开启了

```she
netstat -an|grep3306 
```



没开启就要修改文件，打开

```she
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
```

将里面的

bind-address		= 127.0.0.1  注释掉

然后重启mysql    

```she
 /etc/init.d/mysql restart
```



#### 给mysql开启外链访问权限

进入mysql命令

```shell
 mysql -u root -p 
```

输入

```sql
grant all privileges on *.* to 'root' @'%' identified by *****(密码)
```

改好后刷新权限信息：

```sql
flush privileges; 
```







