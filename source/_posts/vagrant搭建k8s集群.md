---
title: vagrant搭建k8s集群
date: 2019-09-07 21:55:55
tags: [vagrant,k8s,docker]
categories: 后端服务器
---

我使用之前的vagrant工具，启动了ubuntu16.04虚拟机，并且安装了docker,git工具，然后导出了自己的vagrant box文件，命名为docker.box。 然后基于这个box 搭建k8s。

### 宿主机的配置

在合适的地方创建cluster文件夹，然后将docker.box移入这个目录，不移入也没关系，我这是便于管理。然后参考之前的教程，步骤都是一样滴，添加box到vagrant，执行vagrant init  docker，在当前目录生成Vagrantfile，然后修改默认的Vagrantfile，启动虚拟机。

Vagrantfile配置文件改为：

```shell
Vagrant.configure("2") do |config|
  config.vm.box = "docker"
  config.vm.define "master" do |master|  # 第一个虚拟机,名字为master，配置命名为master
        master.vm.network "private_network", ip: "192.168.100.100"
        master.vm.hostname = "master"
        master.vm.provider "virtualbox" do|pmaster|
                pmaster.memory = "1024"
                pmaster.cpus = 1
        end
  end
```



### k8s安装基础环境配置

```shell
# 创建安装源配置文件
sudo touch /etc/apt/sources.list.d/kubernetes.list
# 设置文件可写
sudo chmod 666 /etc/apt/sources.list.d/kubernetes.list
# 添加下载源到文件末尾
sudo cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main
EOF

# 更新源
sudo apt update
# 然而报错了，说 the public key is not available: NO_PUBKEY 6A030B21BA07F4FB

# 添加key的后八位 BA07F4FB 到系统中，这个步骤其实是k8s的安全下载措施

# 在机器上生成证书
sudo gpg --keyserver keyserver.ubuntu.com --recv-keys BA07F4FB
# 添加证书到k8s的环境中
sudo gpg --export --armor BA07F4FB | sudo apt-key add -
# 更新下载源
sudo apt update

# 一般涉及到网络的服务，都会禁止系统自带的安全模块，防止出错
# 禁止系统自带的防火墙unix firewall 
sudo ufw disable
# 关闭系统swap,一般是永久关闭，然而vagrant的ubuntu16.04虚拟机默认就是关闭的
# 在系统文件 /etc/fstab 里没有swap.img的配置，如果有，注释掉就是永久关闭swap了

# 禁止selinux,这是ubuntu自带的类似360的软件,这个我猜不做也可以，
# 不过这个软件影响机器性能，还没什么用，禁了。要禁止它需要下载他的工具selinux-utils
sudo apt install -y selinux-utils
# 关闭它
setenforce 0
# 查看是否已经关闭 selinux
sudo getenforce 

# 重启机器
sudo shutdown  -r now

```



### k8s系统网络环境配置

创建 /etc/sysctl.d/k8s.conf 文件，写入内容：

```shell
# 修改文件可读可写
sudo chmod 666 /etc/sysctl.d/k8s.conf
# 添加内容
sudo cat <<EOF > /etc/sysctl.d/k8s.conf
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
```

与视频不一致，我这个是官网复制的，视频里多了一行    vm.swappiness=0



```shell
# 使用modproble加载网桥透明防火墙
sudo modprobe br_netfilter
# 使配置文件生效
sudo sysctl -p /etc/sysctl.d/k8s.conf
# 安装k8s
sudo apt update
sudo apt-get install kubelet kubernetes-cni kubectl kubeadm
```

vagrant 里如果需要root权限，可能无法授权。可以先sudo passwd root 先给root设定一个密码就可以了。

重启机器，到这个时候，如果执行kubectl get nodes,显示

```shell
The connection to the server localhost:8080 was refused - did you specify the right host or port?
```

这是网络环境还不对，但是命令已经有了。

使用vagrant给机器做个镜像，导出镜像为 k8s.box ，添加k8s.box到 vagrant  box list 里，之后需要根据这个box 部署集群。

#### 集群的机器准备

准备三台虚拟机，分别为master,slave01,slave02

在cluster目录下，创建两个文件夹 master和slave，分别在这两个文件夹下创建Vagrantfile

第一个：

```shell
Vagrant.configure("2") do |config|
  config.vm.box = "k8s"
  config.vm.define "master" do |master|  # 第一个虚拟机，打算作为k8s的master,配置命名为master
        master.vm.network "private_network", ip: "192.168.100.100"
        master.vm.hostname = "master"
        master.vm.provider "virtualbox" do|pmaster|
                pmaster.memory = "1024"
                pmaster.cpus = 1
        end
  end
end

```

第二个

```shell
Vagrant.configure("2") do |config| # 表示使用版本2的配置文件格式启动，配置命名为config，下面可以引用
  config.vm.box = "k8s"  # 指定box list 里的 box 

  config.vm.define "slave01" do |slave01|  # 虚拟机名字为slave01，配置命名为slave01
        master.vm.network "private_network", ip: "192.168.100.101"
        master.vm.hostname = "slave01"
        master.vm.provider "virtualbox" do|pslave01|
                pmaster.memory = "1024"
                pmaster.cpus = 1
        end
   end
   config.vm.define "slave02" do|slave02|
        slave01.vm.hostname = "slave02"
        slave01.vm.network "private_network", ip: "192.168.100.102"
        slave01.vm.provider "virtualbox" do|pslave02|
                pslave01.memory = "1024"
                pslave01.cpus = 1
        end
   end   
   
end
```

启动三个虚拟机，根据各自的ip修改他们的 /etc/hosts,在文件中添加

```shell
192.168.100.100 master
192.168.100.101 slave01
192.168.100.102 slave02
```

使它们可以根据机器名字互相ping通

#### 集群配置文件

登陆master,在～目录下创建working文件夹，然后生成配置文件

```
mkdir working
cd working
kubeadm config print init-defaults ClusterConfiguration > kubeadm.conf
```

配置文件稍作修改

```shell
# 镜像源修改
imageRepository: registry.aliyuncs.com/google_containers  
# 本机ip
advertiseAddress: 192.168.100.100
# 最后的网络
networking:
  dnsDomain: cluster.local
  podSubnet: 10.244.0.0/16  # pod之间的通信网络
  serviceSubnet: 10.96.0.0/12 # pod之间的通信网络
  
```

```shell
# 查看需要下载的模块
kubeadm config images list --config kubeadm.conf

# 下载模块
kubeadm config images pull --config kubeadm.conf

# 初始化生成master
sudo kubeadm init --config ./kubeadm.conf 
```

说明：

1. registry.aliyuncs.com/google_containers/kube-apiserver:v1.16.0  apiserver提供对外接口，外部通过访问apiserver进入到集群中，所以每个kubelet都有这个
2. registry.aliyuncs.com/google_containers/kube-controller-manager:v1.16.0 Controller Manager作为集群内部的管理控制中心，负责集群内的Node、Pod副本、服务端点（Endpoint）、命名空间（Namespace）、服务账号（ServiceAccount）、资源定额（ResourceQuota）的管理，当某个Node意外宕机时，Controller Manager会及时发现并执行自动化修复流程，确保集群始终处于预期的工作状态。
3. registry.aliyuncs.com/google_containers/kube-scheduler:v1.16.0。调度器是一个策略丰富、拓扑感知、工作负载特定的功能，显著影响可用性、性能和容量。调度器需要考虑个人和集体 的资源要求、服务质量要求、硬件/软件/政策约束、亲和力和反亲和力规范、数据局部性、负载间干扰、完成期限等。 工作负载特定的要求必要时将通过 API 暴露。
4. registry.aliyuncs.com/google_containers/kube-proxy:v1.16.0.  主要做内部的负载均衡
5. registry.aliyuncs.com/google_containers/pause:3.1。Kubernetes创建Pod时，首先会创建一个pause容器，为Pod指派一个唯一的IP地址。然后，以pause的网络命名空间为基础，创建同一个Pod内的其它容器（–net=container:xxx）。因此，同一个Pod内的所有容器就会共享同一个网络命名空间，在同一个Pod之间的容器可以直接使用localhost进行通信。
6. registry.aliyuncs.com/google_containers/etcd:3.3.15-0    各个容器数据的内部一致性
7. registry.aliyuncs.com/google_containers/coredns:1.6.2 



上面的模块安装后，会提示执行如下指令

```shell
## 本地环境配置
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config


# 其他机器加入集群，这个提示的token要备份，以后其他机器加入集群都要用
# kubeadm join 192.168.100.100:6443 --token  巴啦啦啦一大堆
```

``` shell
# 开机启动
sudo systemctl enable kubelet
sudo systemctl start kubelet
# 查看命令,这个时候终于有了,但是status还是NotReady，是因为还没有配置内部网络环境
kubectl get node
```

#### 配置集群的内部网络环境flannel

```shell
# 下载一个配置文件
wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
# 修改里面的 net-conf.json选项，网络为 kubeadm.conf配置文件里面的网络
#     {
#       "Network": "10.244.0.0/16",
#       "Backend": {
#        "Type": "vxlan"
#       }
#     }

# 执行命令生效flannel配置
kubeadm apply -f kube-flannel.yml

```

过一会儿，等配置生效。然后执行  kubectl get nodes 命令，status就是ready了。到此，master终于弄好了。

#### 添加其他机器到集群中

将master机器里的两个文件分别复制到slave01和slave02机器里。

 /etc/kubernetes/admin.conf   

~/working/kube-flannel.yml

登陆进入slave01机器，执行

```shell
# 开机启动
sudo systemctl enable kubelet
sudo systemctl start kubelet

# 用admin.conf文件生成k8s配置文件
mkdir -p $HOME/.kube
sudo cp -i admin.conf .kube/config 
sudo chown $(id -u):$(id -g) .kube/config 
# 用mster初始化的时候生成的信息加入到集群中
sudo kubeadm join 192.168.100.100:6443 --token abcde巴拉巴拉一大堆
```

到此为止，slave01已经加入到集群里，过一会儿等它生效，执行  kubectl get nodes 命令，会看到有两台机器，且status是ready了。slave02也是一样的操作。



最后，如果slave机器一直都是notready，还需要配置slave01和slave02可以互相通信，就需要用到master里复制过来的kube-flannel.yml文件了

```shell
kubectl apply -f kube-flannel.yml 
```



#### 总结几个大步骤

1. master机器上kubeadm init --config 创建master，生成配置文件
2. 配置集群的内部通信网络，flannel网络
3. 配置node节点，分配网络
4. 让node加入集群

#### 最后

到此为止，终于部署好三台机器的k8s集群，可以分别给master和node做镜像，防止以后还要重新这样部署一遍，啦啦啦   ：）

