---
title: golang重点笔记
date: 2019-09-23 14:52:35
tags: golang	
---

### go语言的闭包重点

- 闭包不是某一种语言特有的机制,但常出现在函数式编程中,尤其是函数占据重要地位的编程语言.
- 闭包的直观表现是函数内部嵌套了函数,并且内部函数访问了外部变量,从而使得自由变量获得延长寿命的能力.
- 闭包中使用的自由变量一般有值传递和引用传递两种形式,示例中的斐波那契数列生成器利用的是引用而循环变量示例用的是值传递.
- Go不支持函数嵌套但支持匿名函数,语法层面的差异性掩盖不了闭包整体的统一性.

速记：只要函数还被引用着，里面的变量不会被销毁

示例: 斐波那契

```go
func fibonacci() func() int {
  a, b := 0, 1
  return func() int {
    a, b = b, a+b
    return a
  }
}

// 1 1 2 3 5 8 13 21 34 55
func TestFibonacci(t *testing.T) {
  f := fibonacci()
  for i := 0; i < 10; i++ {
    fmt.Print(f(), " ")
  }
  fmt.Println()
}
```

示例：引用传递和值传递

```go
 func countByClosureWithOk() []func() int {
  var arr []func() int
  for i := 1; i <= 3; i++ {
    func(n int) {
      arr = append(arr, func() int {
        return n
      })
    }(i)
  }
  return arr
}

func countByClosureButWrong() []func() int {
  var arr []func() int
  for i := 1; i <= 3; i++ {
    arr = append(arr, func() int {
      return i
    })
  }
  return arr
}


func TestCountByClosure(t *testing.T) {
   // 1 2 3
  for _, c := range countByClosureWithOk() {
    t.Log(c())
  }
  
    // 4 4 4 
  for _, c := range countByClosureButWrong() {
    t.Log(c())
  }
}
```

上面的例子，在使用闭包的时候，都把函数存到了一个变量里，这样整个函数包括里面的变量，都不会被释放，直到不再引用这个变量。这个跟语言的垃圾回收机制有关。





