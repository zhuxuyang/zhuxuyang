---
title: '使用docker部署nginx,并反向代理转发请求'
date: 2018-09-03 18:17:23
tags: [nginx,docker]
categories: 后端服务器
---

### docker版nginx

```shell
# 在～目录创建web文件夹,结构为
# .
# └── web
#    └── nginx
        
## 查询相关镜像
docker search nginx 
#获取容器
docker pull nginx
#跑起来试试
docker run -p 80:80 -d nginx
#容器改个名字
docker rename funny_hypatia nginx
#进入～/web/nginx目录，复制配置文件到本地～/web/nginx
docker cp nginx:/etc/nginx/nginx.conf .
#复制基础网页文件到本地
docker cp nginx:/usr/share/nginx/html ～/web/nginx/www
# 现在调整目录结构为
# └── web
#     ├── nginx
#     │   ├── conf
#     │   │   ├── my_nginx.conf
#     │   │   └── nginx.conf
#     │   └── log
#     └── www
#         └── html
#             ├── 50x.html
#             └── index.html

docker run -d -p 80:80 --name nginx -v ~/web/www/html:/usr/share/nginx/html -v ~/web/nginx/conf/my_nginx.conf:/etc/nginx/nginx.conf -v ~/web/nginx/log:/var/log/nginx nginx
```



### 反向代理

在my_nginx.conf 的http里加上配置，这样配置浏览器访问这台机器，内容显示为百度，测试通过。

```shell
server {
    listen 80;
    server_name  localhost;
 
    location / {
        proxy_pass  http://itwork.group;
   }
}
```

注意server监听的端口，在开启nginx容器的时候需要做端口映射到宿主机上