---
title: docker自动化部署应用
date: 2018-09-24 17:38:40
tags: docker 
---

### 最简单的是使用第三方的服务，daocloud

在daocloud里绑定github，创建项目，绑定github仓库。

代码仓库必须包含Dockerfile，这样每次推送代码到github，就会触发docker build 生成docker镜像。如果需要部署，在应用界面里面的发布选项中选择自动发布就行，这样每次推送代码应用就更新了。这是小型项目最简单的方案了。（有时候频繁发布可能获取不到最新的代码版本，感觉不对的时候可以对比界面上的代码提交版本号）

