---
title: gomod 的使用
date: 2020-10-12 16:04:39
tags:
---

# Go-Mod

## 官网



[gomod wiki](https://github.com/golang/go/wiki/Modules)



## 1.开启go mod



### 设置GO111MODULE环境变量



```
export GO111MODULE=on;
```



### 设置代理



#### SS代理设置



在ss开启的成功下，使用http对应的端口代理。



**MAC:**

```
export http_proxy=http://127.0.0.1:1087;
export https_proxy=http://127.0.0.1:1087;
```



**Windows:**

```
export http_proxy=http://127.0.0.1:1080;
export https_proxy=http://127.0.0.1:1080;
```



#### GoProxy设置



##### 官方支持



```
export GOPROXY=https://proxy.golang.org
```



##### 个人支持



```
export GOPROXY=https://goproxy.io
```



##### 阿里云支持



```
export GOPROXY=https://mirrors.aliyun.com/goproxy
```



##### 七牛云支持



```
export GOPROXY=https://goproxy.cn
```



## 2.Go mod初始化项目



init 之后会生成go.mod



```
go mod init 项目的名字（不加项目名字会报错缺少flag）
// go: creating new go.mod: module ccp-server/blackwordserver
```



## 3.下载依赖包



go build 会展示finding和download包的细节；同时会生成go.sum文件



```
go build -v
```



## 4.依赖转移



go mod vendor会将依赖包挪到项目vendor中



```
go mod vendor
```



## 5.安装指定版本和更新依赖



### 安装指定版本



使用mod修改，也可以直接修改go.mod文件



```
go mod edit -require=github.com/gin-gonic/gin@v1.4.0
```



### 更新版本



```
go get -u github.com/gin-gonic/gin
```



```
go get github.com/gin-gonic/gin@v1.4.0
```



## 6.拉取本地gitlab依赖包



**获取gitlab的`accesstoken`并配置**



进入`Gitlab`—>`Settings`—>`Access Tokens`，然后创建一个`personal access token`，这里权限最好选择只读(read_repository)。



有了access token后，我们还需要在git中进行配置，这样才能go get下了私有仓库的包，需要把刚刚的token添加进git的请求头中，操作如下：



```
git config --global http.extraheader "PRIVATE-TOKEN: 7U7s4JzfZs-FSUpabk2i"
```



**配置git将请求从ssh转换为http**



配置如下:



```
git config --global url."git@git.learn-inc.com".insteadOf"http://git.learn-inc.com"
```



配置完以上的设置其实是在`~/.gitconfig`文件中输入了以下配置



```
[http]
    extraheader = PRIVATE-TOKEN: 7U7s4JzfZs-FSUpabk2i
[url "git@git.learn-inc.com"]
    insteadOf = http://@git.learn-inc.com
```



即可下载gitlab本地依赖包，`-insecure`是在http下支持的选项



```
go get -insecure -v git.learn-inc.com/yuyongbo/repo
```



经过测试，gitlab仓库下的三级目录是无法get下来的，报以下错误,提示上一级非gitrepo。



```
go get git.learn-inc.com/ccp/server/framework: git ls-remote -q origin in d:\gopath\pkg\mod\cache\vcs\97c0b85fbac8e5852ec9c9b1e09dd846ee9e9b9a70d7975886a81d7ea64e40d7: exit status 128:
        remote: The project you were looking for could not be found.
        fatal: repository 'http://git.learn-inc.com/ccp/server.git/' not found
```



于是就使用了`replace`方案，然后 使用`go mod vendor`命令将依赖包从pkg下面转移到了vendor下面，最后直接使用`go build -mod=vendor`进行编译和构建项目。



```
replace git.learn-inc.com/ccp/server/framework => ../framework
```



## 7.其他



### 清除mod缓存



```
go clean -modcache
```



### 查看项目中依赖和情况



```
go test ./...
```



## 8.常见问题以及解决方案



### 1. 无法解析mod，是由于使用了replace



```
go: github.com/gortc/sdp@v0.17.0: parsing go.mod: unexpected module path "gortc.io/sdp"
```



所以编辑go.mod替换下对应库即可



```
require github.com/gortc/sdp v0.16.0
replace gortc.io/sdp v0.16.0 => github.com/gortc/sdp v0.16.0
```



### 2.不能加载包，不能找到mod提供的包



```
build command-line-arguments: cannot load ccp-server/framework/config: cannot find module providing package ccp-server/framework/config
```



这是引用了本地包的报错信息，解决方案有以下几种



- 把引用的包放入自己的项目中
- 放到github上面再拉下来
- 放到gitlab上进行拉取



### 3.运行go build 报错如下解决方案



显示未找到包，然后执行下面的报另外一个错误



![image](https://cdn.nlark.com/yuque/0/2019/png/208437/1563504182671-6778dc92-cb41-4b7b-819f-80e96e8052f4.png)



**解决方案**



参考1



```
export GO111MODULE=on
export GOPROXY=https://goproxy.io
```



### 4.如果go build 提示找不到路径的报错



**解决方案**



1、在goland中的referrence中的Go的Go Modules（vgo）要勾选Enable等选项然后apply



2、进入main函数所在的目录，然后go build



## 9.具体可参考文章



1.[Go Module工程化实践](https://www.gitdig.com/go/#go-项目工程)



- [Go Module 工程化实践（一）: 基础概念篇](https://www.gitdig.com/go-mod-enterprise-work-1/)
- [Go Module 工程化实践（二）: 取包原理篇](https://www.gitdig.com/go-mod-enterprise-work-2/)
- [Go Module 工程化实践（三）: 工程实践篇](https://www.gitdig.com/go-mod-enterprise-work-3/)



2.[10分钟学会go module](https://mp.weixin.qq.com/s/fS8Y20ZUlWkVuAim2uN-iQ)



3.[go moudle使用实践及问题解决](http://blog.ipalfish.com/?p=443)



4.[Golang1.5到Golang1.12包管理：golang vendor 到 go mod](https://studygolang.com/articles/18634)



5.[go get获取gitlab私有仓库的代码](https://mshk.top/2019/01/go-get-gitlab-https/)



6.[go module,使用gitlab私有仓库作为项目的依赖包](https://m.ancii.com/anzua0l0q/)



7.[Go Modules：Go 1.11和1.12引入的依赖包管理方法](https://www.lijiaocn.com/编程/2019/05/05/go-modules.html)