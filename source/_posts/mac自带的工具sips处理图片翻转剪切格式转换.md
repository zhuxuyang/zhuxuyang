---
title: mac自带的工具sips处理图片翻转剪切格式转换
date: 2019-08-15 18:02:33
tags: [sips]
categories: 工具
---

mac上自带一个工具，有时候仅仅是简单的修改一下图片，就不需要额外下载其他工具，很有用。叫做sips，是一个命令行工具，简单的处理图片或者批量处理图片的神器。

***记住，不指定输出文件会覆盖原文件，一定要先备份***

```shell
#裁剪 其中600表示高度为600px,宽度为按比例缩放
sips -Z 600 aa.jpg

#格式转换, 使用 -s 参数可以修改图片格式为指定值，
#sips 支持 jpeg | tiff | png | gif | jp2 | pict | #bmp | qtif | psd | sgi | tga 共 11 种格式。
sips -s format jpeg --out a.jpg aa.png

# 指定宽高输出图片
sips --out output.png -z ${height} ${width} input.png

# 制定宽度、保持比例输出图片
sips --out output.png --resampleWidth ${width} input.png

# 指定宽度、保持比例(会覆盖源图片)
sips --resampleWidth ${width} *.jpg

# 指定宽高(会覆盖源图片)
sips -z ${height} ${width} *.jpg

# 顺时针旋转 90 度
sips -r 90 image_file_name

# 水平翻转图片 
# horizontal/vertical 水平／垂直
sips -f horizontal image_file_name

# 获取图片 meta 信息
sips -g pixelWidth -g pixelHeight image_file_name

# 可使用查看更多信息
man sips

```

