---
title: nginx配置文件服务器
date: 2018-09-25 13:24:36
tags: nginx
categories: 后端服务器
---

#### nginx文件服务器，可以像在本地一样浏览查看

在配置文件里添加

```shell
  server {
      listen 8082;
      server_name *.*.*.*; # 服务器的域名或者ip
      charset utf-8;   # 避免中文乱码
      root /home; # 存放文件的目录,如果使用nginx默认的服务器/usr/share之后的目录，会破坏默认的80端口                    										 服务器，所以这里使用home目录  
      location / {
        autoindex on; # 索引,是否显示文件夹目录
        autoindex_exact_size on; # 显示文件大小，打开关闭是单位为Mb和kb
        autoindex_localtime on; # 显示文件时间
      }
    }
```

浏览器访问对应端口就可以看到目录了，可以放一些文件，方便在其他设备上查看

