---
title: golang 易错知识点
date: 2021-06-19 11:52:04
tags:
---

### 变量的比较

```go
package main
func main() {
  var x interface{}
  var y interface{} = []int{3, 5}
  _ = x == x
  _ = x == y
  _ = y == y // 会panic
}
```



### 函数和方法

方法可以看作是函数的语法糖，方法的所有者可以看作是函数的第一个参数。

所以方法是指针还是引用，和函数的参数一样，会涉及到函数内修改是否会影响到函数外。

