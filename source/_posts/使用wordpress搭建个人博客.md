---
title: 使用wordpress搭建个人博客
date: 2017-11-06 15:56:20
tags: wordpress
categories: 后端服务器
---

此文记录使用wordpress搭建个人博客的步骤

需要一台服务器。手机，电脑，vps等都可以，只要要能装上Linux。

如果是用本地电脑ssh连接服务器，那么本文的localhost 需要替换为服务器ip地址

####  

#### 服务器搭建LAMP环境

LAMP(Linux + Apache + MySQL/MariaDB/Percona + PHP)

依次执行如下命令，安装过程中输入密码等注意看提示。如果已经处于root权限就不需要sudo

安装数据库，^符号必须有

```
sudo apt install lamp-server^
```

安装数据库管理工具

```
sudo apt install phpmyadmin
```

重启数据库和阿帕齐

```
sudo service mysql restart
sudo service apache2 restart
```

浏览器登录<http://localhost/phpmyadmin>看是否出现初始登录界面。这里可以管理数据库。

```
sudo cp -r ./wordpress /var/www/html
cd /var/www/html
sudo chmod -R 777 wordpress 
```

#### 安装WordPress

到<https://cn.wordpress.org/>去下载最新的WordPress中文版本。然后解压到当前目录。

复制解压后的wordpress文件夹到/var/www/html目录下,进入/var/www/html 目录,给wordpress目录赋权，需要所有用户能读写的权限

```
sudo cp -r ./wordpress /var/www/html
cd /var/www/html
sudo chmod -R 777 wordpress 
```

登录<http://localhost/wordpress/> ，出现设置界面表示以上步骤没问题。但是这时候可能进不去，因为数据库和wordpress的配置信息还没有做好。接下来马上做这个。

#### 新建数据库和配置网站

登录<http://localhost/phpmyadmin>，点击数据库，输入数据库名，比如wordpress，然后点击创建。也可以去建一个新用户，然后赋权，但一般还是用root用户就行了。

修改配置文件 ，依次执行如下命令，就是将默认的示例配置文件复制一个，然后修改为你的配置信息

```
cd /var/www/html/wordpress
cp wp-config-sample.php wp-config.php
sudo vi wp-config.php
```

将 wp-config.php文件里的下面配置修改

```
/** WordPress数据库的名称 */
define('DB_NAME', 'wordpress');

/** MySQL数据库用户名 */
define('DB_USER', 'root');

/** MySQL数据库密码 */
define('DB_PASSWORD', '******');

/** MySQL主机,一个机器上就是 localhost ，否则就是ip */
define('DB_HOST', 'localhost');
```

改好后保存退出，浏览器 访问 <http://localhost/wordpress/>，注册网站管理员，妥妥的。

#### 安装插件

如果安装插件，发现需要FTP账号密码，其实可以绕过去。办法还是修改wp-config.php，在文件里添加如下代码。

```
/** 不需要ftp密码直接下载安装插件*/
define("FS_METHOD","direct");

define("FS_CHMOD_DIR", 0777);

define("FS_CHMOD_FILE", 0777);

```

####  