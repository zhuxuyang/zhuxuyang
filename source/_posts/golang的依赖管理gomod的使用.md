---
title: golang的依赖管理gomod的使用
date: 2019-01-09 13:20:26
tags: [golang]
categories: 后端服务器
---

以前的go语言项目，代码依赖是个麻烦的事情。所有的项目必须放在GOPATH的src目录下，如果不同项目依赖同一个库的不同版本，就比较麻烦，不好管理。现在go语言终于有了go mod工具，可以像java一样管理依赖，工程代码可以放在其他目录了。

### 环境变量设置

设置环境变量 GO111MODULE ，值为下面三选一。

- `GO111MODULE=off`，go命令行将不会支持module功能，寻找依赖包的方式将会沿用旧版本那种通过vendor目录或者GOPATH模式来查找。
- `GO111MODULE=on`，go命令行会使用modules，而一点也不会去GOPATH目录下查找。
- `GO111MODULE=auto`，默认值，go命令行将会根据当前目录来决定是否启用module功能。这种情况下可以分为两种情形：
	1. 当前目录在GOPATH/src之外且该目录包含go.mod文件。
   	2. 当前文件在包含go.mod文件的目录下面。

既然默认是自动的，那么这个环境变量一般可以不设置。



### go mod的使用

1. 在`GOPATH 目录之外`新建一个目录，并使用`go mod init` 初始化生成`go.mod` 文件。只要有了这个文件，那么项目就使用了gomod来管理依赖了。

```shell
mkdir testgomod # 项目名叫testgomod
cd testgomod
go mod init testgomod # 初始化go mod ,在当前目录生成go.mod文件
```

这个时候go.mod内容为,还没有任何依赖

```shell
module testmod

go 1.12
```

在testgomod下创建main.go 文件，里面有两个常用依赖，内容为：

```go
package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"log"
)

func main() {
	log.Print(gorm.Model{})
	log.Print(gin.Default())
}

```

这个时候执行go build, gomod 会自动拉取相关依赖包，然后在go.mod里加入依赖信息，go.mod内容为：

```shell
module testmod

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/jinzhu/gorm v1.9.10
)

```

依赖包的源码在$GOPATH/pkg/mod目录下。依赖包的版本可以在go.mod里指定版本号，修改后再次执行

go build，就可以更新依赖。以后就再也不需要go get ./...了。

### go proxy

如果一些依赖无法下载，可以使用go proxy

```shell
export GOPROXY=https://goproxy.io
# 或者使用阿里的
export GOPROXY=https://mirrors.aliyun.com/goproxy/
```

