---
title: 域名指向的apache默认首页改为wordpress网站首页
date: 2017-11-07 15:59:26
tags: [wordpress,apache]
categories: 后端服务器
---

参照之前搭建的wordpress博客，配置好域名之后，浏览器访问 [http://www.itwork.group](http://www.itwork.group/)  出现的是apache的默认页面，而不是博客首页。这个时候输入

http://www.itwork.group/wordpress 才会出现博客首页。然而这不是我想要的。（当然这里我已经改好了）



目标

访问域名可以直接到达博客首页。

实现方法
首先进入wordpress后台的settings,把站点地址（URL）改为域名，比如我的就是[http://www.itwork.group](http://www.itwork.group/)，点保存更改。这个时候还是不能用域名访问的。还要继续配置。

因为我的wordpress是安装在 /var/www/html/wordpress 目录下的。所以进入目录/var/www/html

先备份/var/www/html目录下的index.html文件，防止出错。

1. cp index.hrml index_back.hrml

把wordpress目录下的index.php 复制到外面

1. cp /var/www/html/wordpress/index.php /var/www/html/index.php

用vi修改index.php文件里面的首页文件地址

文件最后一句

1. require( dirname( __FILE__ ) . '/wp-blog-header.php' );

改为

1. require( dirname( __FILE__ ) . '/wordpress/wp-blog-header.php' );vi

改好了保存退出

因为php不需要重新编译，改好了就可以通过域名访问了，妥妥的。