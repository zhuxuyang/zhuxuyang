---
title: ubuntu安装社区版docker-ce
date: 2018-09-03 18:11:50
tags: [ubuntu,docker]
categories: 后端服务器
---

### 服务器基本配置

想都不用想，先更新一发

```shell
sudo apt update
```

安装新版的docker，不使用自带的低版本docker-io，dock er-ce为社区版

```shell
# 添加https支持
sudo apt install apt-transport-https ca-certificates curl software-properties-common
#添加软件安装源
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt update
apt-cache policy docker-ce
## 设置版本为stable的
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get install docker-ce
docker 
```



#### 国内环境可能下载比较慢，这样搞

阿里云脚本安装

```shell
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```

阿里云镜像手动安装

```shell
# step 1: 安装必要的一些系统工具
sudo apt-get update
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
# step 2: 安装GPG证书
curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
# Step 3: 写入软件源信息
sudo add-apt-repository "deb [arch=amd64] http://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
# Step 4: 更新并安装 Docker-CE
sudo apt-get -y update
sudo apt-get -y install docker-ce
```

#### 创建docker用户组，并添加当前用户来使用权限

执行docker命令，显示connect: permission denied，那么创建docker用户组比较科学，直接sudo 不符合安全规范。

```shell
# 创建用户组
sudo groupadd docker 
# 当前用户添加到docker用户组
sudo usermod -aG docker $USER
```

#### docker镜像源修改

创建    /etc/docker/daemon.json文件，内容为

```jso
{
"registry-mirrors": ["http://hub-mirror.c.163.com"]
}
```

```shell
# 重新加载配置文件
sudo systemctl daemon-reload
# 重启docker
sudo systemctl restart docker
```

